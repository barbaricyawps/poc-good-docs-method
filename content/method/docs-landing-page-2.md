---
title: How to structure and present a documentation landing page
Author:
  - "Anita Ihuman"
image: images/method/docs-landing-page-2.jpg
slug: "docs-landing-page-part-2"
photoCredit: "Photo by [Lauren Mancke](https://unsplash.com/@laurenmancke?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) on [Unsplash](https://unsplash.com/)"
date: "2023-11-10"
summary: How to create your docs landing page.
draft: false
---


A documentation landing page for every project exists to capture the user's attention and narrow it down to something. So a documentation landing page can be categorized in various ways and for different purposes:

| By product |  Multiplatform |  Use case|  Critical User Journey (CUJ)|
| -------- | -------- | -------- | --------  |
|There are some projects that offer more than one product, and each have documentation as complex as a stand-alone project. In a situation like this, one can develop a documentation landing page for each of these products.   | When a product works differently on multiple platforms, with each configuration document having a lot of information, a documentation landing page can be implemented to categorize the product configuration based on the various platforms.  | For a product with multiple use cases targeting different audiences and user needs. A landing page can be implemented to help map out the use cases for which a project can be used and help users navigate to the use case that best solves their problems.   | Other projects that have complex documentation can categorize their documentation landing page according to the content type (i.e., reference pages, tutorials, integrations, concept docs) and user needs.  |


## Elements of a documentation landing page

The elements of a documentation landing page will vary depending on the specific product and the needs of its users. However, some common elements that may be included on a documentation landing page are:

* Search functionality
* A brief overview of the product and its features.
* A version comparison section(optional)
* A headline and a sub-headline (optional).
* Language translation
* An option to toggle between dark and light themes
* Contact information to allow users to get in touch with support or ask questions about the product.
* A feedback or comment section
* Graphics and imagery (optional)
* Links to user guides, tutorials, FAQs, and other helpful resources
* Additional links to relevant documentation, such as SDK, API’s or developer guides.

## Steps required for a documentation landing page

### Set clear objectives

Before you begin developing your documentation landing page, you should consider why you are establishing one in the first place. What do you hope to accomplish with this documentation landing page? What kind of experience should the document be based on? Do you need to translate the content into other languages or localize it?

Having a clear idea of who the documentation landing page is for and its purpose can help you remember what to put on it.

### Do your research

To create a successful documentation landing page, you must first acquire information about your consumers and their individual needs by conducting extensive research.
You can do that by collaborating with:

* Your UX team to explore user needs and how they interact with the product (s).
* Your data analytics teams to establish which materials are most beneficial to your users.
* Your support team to establish which issues are most frequently experienced by users so that you can send them to the right documentation that will minimize support calls.
* Your product manager and marketing team to better understand what components of the tool should be highlighted and what "narrative" you should be telling users about your product.

### Provide web graphics and design layout

The web graphics and design layout of a documentation landing page are important elements that can impact the user experience and the overall effectiveness of the page.

* Use images and other visual elements
* Consider intuitive designs that are easy to navigate.
* Consider the overall aesthetic of the page and how it will be perceived by users.

### Write content

The results from the research will help you understand what content your documentation landing page should have.

* Create content outline for the overall page
* Write the content for the page
* Consider the language and structure the information.

### Test for usability

One way to verify if your documentation landing page is fulfilling its purpose is to test for usability through user testing. This involves:

* recruiting a group of representative users to examine and provide feedback based on their experience.
* conducting heuristic evaluation based on established usability principles.
* Using analytics tools to track user behavior on the page.
* You can also include a feedback system on the landing page.
It may be beneficial to use a combination of these approaches in order to get a comprehensive understanding of the page's usability.

## What to consider when creating a documentation landing page

### Project brief

A landing page that responds to each of these queries gives users a summary of the project's scope and offers answers to their needs and demands.


| **What?** | **Who?** | **Why?** | **How?** | **Where?** |
| -------- | -------- | -------- |-------- | -------- |
| What is it the product all about? | Who is this product targeted at?   | Why is it different?   | How does it work?   | Where do users start?  |
| What makes it better than the rest | Who can benefit from this product? | Why should a user consider it?   | How does it help users?   | Where do they go next?   |
| What benefits does your product offer?|    |     |  How can users adopt and utilize the product?    |  Where can they find additional resources? |
|What software and technologies doe it offer?|      |      |     |     |


### Page content

* What content are you prioritizing ?
* Is the content useful to the audience?
* Does it give users reason to believe and trust your product?
* Does it highlight key features?
* Does it utilize the right SEO?
* Does it answer the users frequently asked questions?


### Project size

* Does the product have a sub product
* Are there other features that exist as product options?
* Are the products documentation unique to certain platform or personas?
* Does the product require a pricing plan?

## Best practice for creating a documentation landing page
**User research**
What you know about the user's experience with your product and what they actually experience can be two very different things. Hence, you should work directly with the UX team, product/marketing team, and support team in order to derive a more accurate snapshot of the user's journey.

**Keep it clear and concise**
“Readers come to your documentation looking for answers to the question that's burning in their minds: “Will this help me?”. (Doc for Developers 57–58). Hence, your documentation landing page should be designed in a way that allows users to skim through the page.

**Navigation**
To enable easy navigation between hierarchy levels:

* Consider using a drop-down table of contents.
* Segment your components to support user personas (beginners or experts).
* Consider accessibility (hyperlinks, cross-links, captions, understandable headings, alternative text to images).
* Include a search bar.

**Include visual elements**
Consider using icons, images, pictures, diagrams, graphics, or references that draw the reader's attention. Use tables, illustrations, callouts, and other forms of graphics to depict context, emphasize significant information, and promote structural division.

**Continuously maintain**
As your product or service evolves, so does the customer's onboarding experience. Even a slight tweak, like adding a new command to the installation guide, can become a significant roadblock for users. Hence, you should frequently update the documentation landing page as the product grows,  eliminating obsolete material, correcting errors, and adding new information if needed.

**Measure for success**
The success of a documentation landing page may be measured by how effectively it meets the user's needs. So, employ ways to track user journeys and how they engage with the documentation landing page, using metrics like:

* Number of page views
* Time on page
* Bounce rate
* Number of conversions

Regardless of the examples provided in this guide, your website's landing page should primarily emphasize services that you feel would benefit users the most.

**Additional Resources**

* [Docs for Developers ](https://docsfordevelopers.com/)by Jared Bhatti, Zach Corleissen, Jen Lambourne, David Nuñez, Heidi Waterhouse
* [Microsoft documentation landing page](https://learn.microsoft.com/en-us/business-applications-release-notes/april18/microsoft-flow/new-documentation-landing-page) by Microsoft
* [Splunk redesign](https://www.splunk.com/en_us/blog/tips-and-tricks/docs-splunk-com.html) by Christopher Gales
* [Landing pages](https://docs.readme.com/main/docs/landing-page) by ReadMe

