---
title: Landing Page
Author:
  - "Anita Ihuman"
image: images/method/docs-landing-page-1.jpg
slug: "docs-landing-page-part-1"
photoCredit: "Photo by [Scott Graham](https://unsplash.com/@homajob?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) on [Unsplash](https://unsplash.com/)"
date: "2023-11-09"
summary: A step-by-step approach to creating a documentation landing page.
draft: false
---

## Introduction
A documentation landing page is a type of product documentation that serves as a starting point for potential users to find the information they need to gain a better understanding of the product, its features, and how to use it.

## Proposed audience
The documentation landing page is intended for all users who are looking for information on how to use a product, troubleshoot issues, or gain a better understanding of the features and functionalities.

## Benefits of a documentation landing page
Documentation is frequently used to market a product further down the sales funnel, especially when clients are more serious about determining whether or not to acquire a product. A documentation landing page can be used to:

* Focus a reader's attention.
* Deliver a faster "time-to-value".
* Introduce one more navigation that complements the TOC and improves user experience.
* Improve the organization and structure of the product's documentation.
* Increase your website's visibility and attract more potential customers or users.

## Contrasting a docs landing page with product marketing landing pages and home pages
A documentation landing page is often confused with so many other documentation pages, like the product marketing landing page and home page.

The following table describes the differences:

| | Documentation landing page | Product marketing landing page | Documentation home page |
|----| ---- | -------- | ----- |
| Purpose   |   Serves as a convenient access point for interested users.   | Provides an overview of the project documentation and supports information architecture goals.   | Provides a call to action and is specific to marketing or advertising efforts  |
| Scope  |   Redirects the user's focus toward their primary goals, which could be: how to install it and how it benefits them.  |   Serves as the entry point to a larger documentation site and links to every other crucial permanent page in the document.  |   Exists as a click-through or lead-generation page that leads to other pages.   |
|   Audience |  Technical users who have an interest in understanding and using the product.   |    Visitors who are curious about the product and wish to know more.   |   Business decision-makers who need to make strategic decisions on whether to adopt the product.   |



### When you need a documentation landing page
Although a documentation landing page is an essential document, not every project requires one. In most cases, where a product has fewer documents, a simple navigation menu or table of contents can suffice. The documentation landing page can exist for various categories of conditions:
* When a single product's documentation contains a lot of information
* When a product offers more than one tool or more than one documentation set
* In small scale projects, it can be paired with the home page.

